module.exports = function (context, req) {

    /*
    * Azure Function for improved formatting of PagerDuty -> Microsoft Teams webhook integrations
    * Sindre Lindstad, 2018
    */

    var teamsWebhookSite = "outlook.office.com"; // outlook.office.com
    var teamsWebhookPath = "/webhook/xyz"; // webhook path excl. hostname

    var iconUrl = "https://slack-files2.s3-us-west-2.amazonaws.com/avatars/2016-06-09/49671169684_cbdc45293ab75ea06413_192.png";
    var triggeredColor = "d72b62"; // d72b62
    var acknowledgedColor = "f6c14f"; // f6c14f
    var resolvedColor = "008c4c"; // 008c4c
    var titleMaxLength = 80;
    var descriptionMaxLength = 500;

    if (req.body) {
        
        var incidentTitle = "#" + req.body.messages[0].incident.incident_number + ": " + req.body.messages[0].incident.title.substring(0, titleMaxLength);
        var incidentTitleSplit = incidentTitle.split('http');
        incidentTitle = incidentTitleSplit[0];

        var incidentDescription = req.body.messages[0].incident.description.substring(0, descriptionMaxLength);

        payload = JSON.parse(jsonModelTriggered);                                                                                                                 

        if(req.body.messages[0].event == "incident.trigger") // Triggered
        { 
            payload.summary = "Triggered " + incidentTitle;
            payload.title = "Triggered " + incidentTitle;
            payload.sections[0].text = incidentDescription;
            payload.themeColor = triggeredColor;
            payload.sections[0].activityTitle = req.body.messages[0].incident.service.name;
            payload.sections[0].activitySubtitle = "Created: " + req.body.messages[0].incident.created_at;
            payload.sections[0].activityImage = iconUrl;
            payload.sections[0].facts[0].value = req.body.messages[0].incident.escalation_policy.summary;
            payload.potentialAction[0].targets[0].uri = req.body.messages[0].incident.html_url;

        } 
        else if (req.body.messages[0].event == "incident.acknowledge") // Acknowledged
        { 
            payload = JSON.parse(jsonModelAckResolved);
            payload.summary = "Acknowledged " + incidentTitle;
            payload.title = "Acknowledged " + incidentTitle;
            payload.themeColor = acknowledgedColor;
            payload.sections[0].text = "Acknowledged by " + req.body.messages[0].incident.last_status_change_by.summary;

        } 
        else if (req.body.messages[0].event == "incident.resolve") // Resolved
        { 
            payload = JSON.parse(jsonModelAckResolved);
            payload.summary = "Resolved " + incidentTitle;
            payload.title = "Resolved " + incidentTitle;
            payload.themeColor = resolvedColor;
            payload.sections[0].text = "Resolved by " + req.body.messages[0].incident.last_status_change_by.summary;
        }

        var postData = JSON.stringify(payload);

        // POST to Teams endpoint
        const https = require('https');
        var options = {
            hostname: teamsWebhookSite,
            port: 443,
            path: teamsWebhookPath,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': postData.length
            }
        };

        var req = https.request(options, (res) => {
            context.log('Status code:', res.statusCode);

            res.on('data', (d) => {
                context.log(d);
            });
        });

        req.on('error', (e) => {
            context.res = {
                status: 400,
                body: "Invalid request"
            };
        });

        req.write(postData);
        req.end();

        context.res = {
            // 200 OK
            body: "Incident processed"
        };
    }
    else {
        context.res = {
            status: 400,
            body: "Invalid request"
        };
    }
    context.done();
};

var jsonModelTriggered = '{"@type": "MessageCard","@context": "http://schema.org/extensions","summary": "","themeColor": "d72b62","title": "","sections": [{"activityTitle": "","activitySubtitle": "","activityImage": "","facts": [{"name": "Escalation policy:","value": ""}],"text": ""}],"potentialAction": [{"@type": "OpenUri","name": "View in PagerDuty","targets": [{ "os": "default", "uri": "#" }]}]}';
var jsonModelAckResolved = '{"@type": "MessageCard","@context": "http://schema.org/extensions","summary": "","themeColor": "008c4c","title": "","sections": [{"text": ""}]}';